from flask import Flask, send_file, send_from_directory
app = Flask(__name__, static_folder='client/build/static')


@app.route('/')
def index():
    return send_file('client/build/index.html')


@app.route('/<path:path>')
def build_dir(path):
    return send_from_directory('client/build', path)
